import { Routes } from '@angular/router';
import { TableLayoutComponent } from './table-layout/table-layout.component';
import { TableExampleComponent } from './table-example/table-example.component';

export const TABLE_COMPONENTS = [
    TableExampleComponent,
    TableLayoutComponent
];

export const TABLE_ROUTES: Routes = [
    {
        path: '', component: TableExampleComponent
    },
    {
        path: 'list', component: TableExampleComponent
    }
];
