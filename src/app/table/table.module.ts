import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TABLE_ROUTES, TABLE_COMPONENTS } from './table.routes';
import {PagerService} from './table.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
       RouterModule.forChild(TABLE_ROUTES),
    ],
    declarations: [
        ...TABLE_COMPONENTS
        ],
        providers: [PagerService]
})

export class TableModule { }
