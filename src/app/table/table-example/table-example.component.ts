import { Component, OnInit } from '@angular/core';
import { LookupService } from '../../providers/lookup.service';
import { TableDataInterface } from '../helper';
import { HttpClient } from '@angular/common/http';
import { PagerService } from '../table.service';
@Component({
  selector: 'app-table-example',
  templateUrl: './table-example.component.html',
  styleUrls: ['./table-example.component.scss']
})
export class TableExampleComponent {
  data: TableDataInterface[] = [];
  keys: string[];
  rowsPerPage: number = 10;
  pager: any = {};
  pagedItems: any[];

  constructor(private lookupService: LookupService, private pagerService: PagerService) {
    this.data = this.lookupService.get();
    this.keys = Object.keys(this.data[0]);
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    this.pager = this.pagerService.getPager(this.data.length, page, this.rowsPerPage);
    this.pagedItems = this.data.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
}
