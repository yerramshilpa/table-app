import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { TableDataInterface } from '../helper';
import {PagerService} from '../table.service';

@Component({
  selector: 'app-table-layout',
  templateUrl: './table-layout.component.html',
  styleUrls: ['./table-layout.component.scss']
})
export class TableLayoutComponent implements OnChanges {
  @Input() data: TableDataInterface[] = [];
  @Input() columnHeaders: string[];
  @Input() pageSize: number;

  pager: any = {};

  pagedItems: any[];
  constructor(private pagerService: PagerService) {
  }

  ngOnChanges() {
    this.setPage(1);
  }
  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    this.pager = this.pagerService.getPager(this.data.length, page, this.pageSize);
    this.pagedItems = this.data.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
}
