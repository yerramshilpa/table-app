import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { APP_ROUTES } from './app-routing.module';
import { AppComponent } from './app.component';
import { TableModule } from './table/table.module';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LookupService, LookupModule } from './providers/lookup.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(APP_ROUTES, {
      useHash: true,
      enableTracing: false
    }),
    TableModule
  ],
  providers: [
    LookupService,
    LookupModule.init(),
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
